<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function utama(){
        return view('welcome');
    }

    public function bio(){
        return view('halaman.biodata');
    }

    public function kirim(Request $request){
        $nama1 = $request['nama1'];
        $nama2 = $request['nama2'];

        return view('halaman.home', ['nama1' => $nama1, 'nama2' => $nama2]);

        // $nama = $request['nama'];
        // $alamat = $request['alamat'];
        // $jeniskelamin = $request['jk'];

        // return view('halaman.home', ['nama' => $nama, 'alamat' => $alamat, 'jeniskelamin' => $jeniskelamin]);
    }
}

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/biodata', [HomeController::class, 'bio']);
Route::post('/kirim', [HomeController::class, 'kirim']);

Route::get('/table', function(){
    return view('halaman.table');
});

Route::get('/data-table', function(){
    return view('halaman.data-table');
});

//CRUD Kategori

//Create Data
//Route untuk mengarah ke form form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);

//Route untuk menyimpan data inputan ke dataBase
Route::post('/cast', [CastController::class, 'store']);

//Read
//Menampilkan semua data pada DataBase
Route::get('/cast', [CastController::class, 'index']);

//detail berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//update
//route untuk mengarah  ke form update  berdasarkan id
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//untuk update  data berdasarkan id id database
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//delete
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
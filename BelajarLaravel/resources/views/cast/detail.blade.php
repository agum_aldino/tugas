@extends('layout.master')

@section('title')
Halaman Detail Cast
@endsection

@section('content')

    <h1 class="text-primary">{{$cast->nama}}</h1>
    <h3 class="text-primary">{{$cast->umur}}</h3>
    <p>{{$cast->bio}}</p>

    <a href="/cast" class="btn btn-secondary my-2">Kembali</a>

@endsection
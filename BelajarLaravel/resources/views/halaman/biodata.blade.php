@extends('layout.master')

@section('title')
Buat Account Baru!
@endsection

@section('content')
<h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf
        <label>First Name</label> <br> <br>
        <input type="text" name="nama1"> <br> <br>

        <label>Last Name</label> <br> <br>
        <input type="text" name="nama2"> <br> <br>

        <label>Gender:</label> <br> <br>
        <input type="radio" name="jk" value="1">Male <br>
        <input type="radio" name="jk" value="2">Female <br> <br>

        <label>Nationality:</label> <br><br>
        <select name="WN">
            <option value="Indonesian">Indonesian</option>
            <option value="Singapuran">Singapuran </option>
            <option value="Malaysian">Malaysian</option>
            <option value="American">American</option>
        </select> <br><br>

        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>

        <label>Bio:</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="kirim">
        
        <!-- <label>Nama</label> <br>
        <input type="text" name="nama"> <br>
        <label>Alamat</label> <br>
        <textarea name="alamat" id="" cols="30" rows="10"></textarea> <br>
        <label>Jenis Kelamin</label> <br>
        <input type="radio" name="jk" value="1">Laki-laki
        <input type="radio" name="jk" value="2">Perempuan <br>
        <input type="submit" value="kirim"> -->
    </form>
@endsection